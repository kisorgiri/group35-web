// REDUX ===> State management Tool
// centralized state

// Web Architechture
// MVC architecture ==> 
// concerns ==> Models, Views, Controller
// bidirectional data flow between models and controller

// FLUX architecture 
// concerns  ====> Views, Actions, Dispatchers, Store
// unidirectional data flow
// Views(UI)==> Actions ===> Dispatchers ===> store
// store holds the logic to update the centralized state
// there can be multiple store

// REDUX architecture
// redux is on top of flux
// concerns  ===> Views, Actions, Reducers, store
// unidirectional data flow
// views===> Actions ===> reducers ===> store
// reducers holds logic to update store
// there is single store

// pre requisities
// library
// redux
// react-redux
// middleware ==> redux-thunk ,redux-saga


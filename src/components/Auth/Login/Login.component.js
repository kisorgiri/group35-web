import React, { Component } from 'react';
import { SubmitButton } from '../../Common/Button/Button.component';
import { Link } from 'react-router-dom';
import { notify } from '../../../utils/toastr';
import { handleError } from '../../../utils/errorHandler';
import { httpClient } from '../../../utils/httpClient';

// syntax of class
// class Name extends React.Component{}
const defaultForm = {
    username: '',
    password: ''
}
const validationFields = {
    username: false,
    password: false

}
export class Login extends Component {

    constructor() {
        super();
        console.log('this.props >>', this.props)
        // to maintain data within a component use state
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...validationFields
            },
            isValidForm: '',
            remember_me: false,
            isSubmitting: false
        }
    }
    componentDidMount() {
        const remember_me = localStorage.getItem('remember_me') && JSON.parse(localStorage.getItem('remember_me'));
        if (remember_me) {
            this.props.history.push('/dashboard/home')
        }
    }

    // bind call and apply
    onSubmit(e) {
        e.preventDefault();  // prevent default behaviour 
        console.log('this.state >>', this.state)
        const isValidForm = this.validateFields();
        if (!isValidForm) return;
        this.setState({
            isSubmitting: true
        })

        // API call
        // data send
        // data extraction from state
        // on success navigate to dashboard
        // this.props.history.push('/dashboard/kishor')
        httpClient
            .POST(`/auth/login`, this.state.data)
            .then(response => {
                console.log('resonse is >>', response);
                // notification
                notify.showSuccess(`Welcome ${response.data.user.username}`)
                // localstorgae setup
                localStorage.setItem('user', JSON.stringify(response.data.user));
                localStorage.setItem('token', response.data.token)
                localStorage.setItem('remember_me', this.state.remember_me);
                // navigate to dashboard
                this.props.history.push('/dashboard');
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })

        // setTimeout(() => {
        //     notify.showSuccess('Login successfull')
        //     // web storage
        //     localStorage.setItem('remember_me', JSON.stringify(this.state.remember_me));
        //     this.props.history.push({
        //         pathname: '/dashboard/test',
        //         state: {
        //             data: ['apple', 'banana']
        //         }
        //     })
        // }, 1000)


    }

    handleChange = (e) => {
        // 
        const { type, checked, name, value } = e.target;
        if (type === 'checkbox') {
            return this.setState({
                remember_me: checked
            });
        }
        this.setState((preState) => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            if (this.state.error[name]) {
                this.validateFields();
            }
        })
    }

    validateFields = () => {
        let validForm = true;
        let usernameErr = false;
        let passwordErr = false;

        if (!this.state.data['username']) {
            usernameErr = true;
            validForm = false;
        }
        if (!this.state.data['password']) {
            passwordErr = true;
            validForm = false;
        }

        this.setState({
            error: {
                username: usernameErr,
                password: passwordErr
            }
        })

        return validForm;

    }

    render() {
        // render method is mandatory inside class based component
        // render must return single html node
        // try to keep UI logic inside render
        return (
            <div className="auth-box">
                <h2>Login</h2>
                <p>Please Login to start using our app</p>
                <form onSubmit={this.onSubmit.bind(this)} className="form-group">
                    <label htmlFor="username">Username</label>
                    <input type="text" name="username" placeholder="Username" id="username" onChange={this.handleChange} className="form-control"></input>
                    <p className="error">{this.state.error.username && `required field*`}</p>
                    <label htmlFor="password">Password</label>
                    <input type="password" placeholder="Password" name="password" id="password" onChange={this.handleChange} className="form-control"></input>
                    <p className="error">{this.state.error.password && `required field*`}</p>
                    <input type="checkbox" onChange={this.handleChange} name="remember_me"></input>
                    <label> &nbsp;Remember Me</label>
                    <hr />
                    <SubmitButton
                        enabledLabel="Login"
                        disabledLabel="Logining in..."
                        isSubmitting={this.state.isSubmitting}
                    >
                    </SubmitButton>
                </form>
                <p>Don't have an account?</p>
                <p style={{ float: 'left' }}>Register
                    <Link to="/register"> &nbsp;here</Link>
                </p>
                <p style={{ float: 'right' }}> <Link to="/forgot_password">forgot password ? </Link></p>

            </div>
        )
    }
}

import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { handleError } from '../../../utils/errorHandler';
import { httpClient } from '../../../utils/httpClient';
import { notify } from '../../../utils/toastr';
import { SubmitButton } from '../../Common/Button/Button.component';

export default class ForgotPassword extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            emailErr: '',
            isSubmitting: false
        }
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        }, () => {
            this.validateForm();
        })
    }

    onSubmit = e => {
        e.preventDefault();
        const isValidForm = this.validateForm();
        if (!isValidForm) return;
        this.setState({
            isSubmitting: true
        })
        httpClient.POST('/auth/forgot-password', { email: this.state.email })
            .then(response => {
                notify.showInfo("Password Reset Link sent to your email please check inbox");
                this.props.history.push('/')
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    validateForm() {
        let emailErr = this.state.email
            ? this.state.email.includes('@') && this.state.email.includes('.com')
                ? ''
                : 'invalid email'
            : 'required field*'

        this.setState({
            emailErr
        })
        const isValidForm = emailErr ? false : true;
        return isValidForm;
    }

    render() {
        return (
            <div className="auth-box">
                <h2>Forgot Password</h2>
                <p>Please provide your registered email to reset password</p>
                <form onSubmit={this.onSubmit} className="form-group" noValidate>
                    <label>Email</label>
                    <input name="email" placeholder="email address here" type="text" className="form-control" onChange={this.handleChange}></input>
                    <p className="error">{this.state.emailErr}</p>
                    <hr />
                    <SubmitButton
                        isSubmitting={this.state.isSubmitting}
                    > </SubmitButton>
                </form>
                <p>back to  <Link to="/">login</Link></p>
            </div>
        )
    }
}

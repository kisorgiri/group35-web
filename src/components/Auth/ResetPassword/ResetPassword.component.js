import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { handleError } from '../../../utils/errorHandler'
import { httpClient } from '../../../utils/httpClient'
import { notify } from '../../../utils/toastr'
import { SubmitButton } from '../../Common/Button/Button.component'

const defaultForm = {
    password: '',
    confirmPassword: ''
}
export class ResetPassword extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false
        }
    }

    componentDidMount() {
        this.token = this.props.match.params['token'];
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name, 'change')
        })
    }

    validateForm = (fieldName, type = 'change') => {
        if (type === 'submit') {
            let passwordErr;
            let confirmPasswordErr;
            let validForm = true;

            if (!this.state.data['password']) {
                passwordErr = 'required field*'
                validForm = false;
            }
            if (!this.state.data['confirmPassword']) {
                confirmPasswordErr = 'required field*'
                validForm = false;
            }
            this.setState({
                error: {
                    password: passwordErr,
                    confirmPassword: confirmPasswordErr
                }
            });
            return validForm;
        }


        let errMsg;
        switch (fieldName) {
            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['confirmPassword']
                        ? this.state.data['confirmPassword'] === this.state.data[fieldName]
                            ? ''
                            : 'password didnot match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'weak password'
                    : 'required field*'
                break;
            case 'confirmPassword':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['password']
                        ? this.state.data['password'] === this.state.data[fieldName]
                            ? ''
                            : 'password didnot match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'weak password'
                    : 'required field*'
                break;
        }


        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {

        })

    }


    handleSubmit = e => {
        e.preventDefault();
        const isValidForm = this.validateForm(null, 'submit');
        if (!isValidForm) return;
        this.setState({
            isSubmitting: true
        })
        httpClient
            .POST(`/auth/reset-password/${this.token}`, this.state.data)
            .then(response => {
                notify.showSuccess("Password Reset Successfull please login");
                this.props.history.push('/');
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        return (
            <div className="auth-box">
                <h2>Reset Password</h2>
                <p>Please choose your password wisely</p>
                <form className="form-group" noValidate onSubmit={this.handleSubmit}>
                    <label>Password</label>
                    <input type="password" placeholder="Password" name="password" onChange={this.handleChange} className="form-control"></input>
                    <p className="error">{this.state.error.password}</p>
                    <label>Confirm Passowrd</label>
                    <input type="password" placeholder="Confirm Password" name="confirmPassword" onChange={this.handleChange} className="form-control"></input>
                    <p className="error">{this.state.error.confirmPassword}</p>

                    <hr />
                    <SubmitButton
                        isSubmitting={this.state.isSubmitting}
                    ></SubmitButton>
                </form>
                <p>back to <Link to="/">login</Link></p>

            </div>
        )
    }
}

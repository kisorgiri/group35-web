import React from 'react';
import { SubmitButton } from '../../Common/Button/Button.component';
import { Link } from 'react-router-dom'
import { notify } from '../../../utils/toastr';
import { handleError } from '../../../utils/errorHandler';
import { httpClient } from './../../../utils/httpClient';

const defaultForm = {
    name: '',
    email: '',
    username: '',
    password: '',
    confirmPassword: '',
    gender: '',
    phoneNumber: '',
    dob: ''
}

const specialCharacters = ['_', '&', '!', '~'];

export class Register extends React.Component {
    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isValidForm: false,
            isSubmitting: false,
        }
    }
    // componentDidMount() {
    //     console.log('component is fully loaded')
    //     // access props here
    //     // API call
    //     // data preparation

    // }

    // componentDidUpdate(prevProps, prevState) {
    //     // console.log('either props or state is modified', this.state)
    //     console.log('old State >>', prevState.data);
    //     console.log('new state ..', this.state.data)
    //     // changes herera action tirgger garne
    // }

    // componentWillUnmount() {
    //     console.log('once component is destroyed')
    //     clearInterval(this.acd)

    // }

    handleChange = e => {
        let { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }
    validateForm = (fieldName) => {
        let errMsg;
        switch (fieldName) {
            case 'username':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].length > 6
                        ? ''
                        : 'Username must be 6 characters long'
                    : 'required field*'
                break;
            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['confirmPassword']
                        ? this.state.data['confirmPassword'] === this.state.data[fieldName]
                            ? ''
                            : 'password didnot match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'weak password'
                    : 'required field*'
                break;
            case 'confirmPassword':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['password']
                        ? this.state.data['password'] === this.state.data[fieldName]
                            ? ''
                            : 'password didnot match'
                        : this.state.data[fieldName].length > 8
                            ? ''
                            : 'weak password'
                    : 'required field*'
                break;

            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                        ? ''
                        : 'invalid email'
                    : 'required field*'
                break;

            default:
                break;
        }

        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            // console.log('thisstate eoor', this.state.error)
            // check weather form has error or not
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    requiredValidation = () => {
        const requiredFields = ['username', 'password', 'confirmPassword', 'email'];
        requiredFields.forEach((item, index) => {
            this.setState(preState => ({
                error: {
                    ...preState.error,
                    [item]: this.state.data[item]
                        ? ''
                        : 'required field'
                }
            }))
        })
        // TODO should return boolean to restrict further action on submit button click
    }

    handleSubmit = e => {
        e.preventDefault();

        const isOk = this.requiredValidation();
        // if (!isOk) return; // TODO
        this.setState({
            isSubmitting: true
        })
        httpClient
            .POST(`/auth/register`, this.state.data)
            .then(response => {
                notify.showSuccess("Registration Successfull,Please Login!")
                this.props.history.push('/');
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })

    }

    render() {
        return (
            <div className="auth-box">
                <h2>Register</h2>
                <p>Please Provide necessary details to register</p>
                <form onSubmit={this.handleSubmit} className="form-group">
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Name" className="form-control" onChange={this.handleChange}></input>
                    <label>Username</label>
                    <input type="text" name="username" placeholder="Username" className="form-control" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.username}</p>
                    <label>Password</label>
                    <input type="password" name="password" placeholder="Password" className="form-control" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.password}</p>

                    <label>Confirm Password</label>
                    <input type="password" name="confirmPassword" placeholder="Confirm Password" className="form-control" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.confirmPassword}</p>

                    <label>Email</label>
                    <input type="text" name="email" placeholder="Email" className="form-control" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.email}</p>

                    <label>Phone Number</label>
                    <input type="number" name="phoneNumber" className="form-control" onChange={this.handleChange}></input>
                    <label>Gender</label>
                    <br />
                    <input type="radio" name="gender" value="male" onChange={this.handleChange} />Male &nbsp;
                    <input type="radio" name="gender" value="female" onChange={this.handleChange} />Female &nbsp;
                    <input type="radio" name="gender" value="others" onChange={this.handleChange} />Others &nbsp;
                    <br />
                    <label>D.O.B</label>
                    <input type="date" name="dob" className="form-control" onChange={this.handleChange}></input>
                    <hr />
                    <SubmitButton
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}

                    >
                    </SubmitButton>
                </form>
                <p>Already Registered? <Link to="/">back to login</Link></p>
            </div>
        )
    }
}


// component life cycle
// 3 stage 
// initilizatation (init)
// constructor,render

// update state
// destroy

// react builtin methods for life cycle hooks
// componentDidMount(); // once component is fully loaded

// componentDidUpdate() // self invoked function it will execute on update
// either props change or state change


// componentWillUnmount(); // once component life cycle is over

import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import React from 'react';
import { Login } from './Auth/Login/Login.component';
import { Register } from './Auth/Register/Register.component';
import { Header } from './Common/Header/Header.component';
import { Sidebar } from './Common/Sidebar/Sidebar.component';
import { AddProduct } from './Products/AddProduct/AddProduct.component';
import { ViewProducts } from './Products/ViewProducts/ViewProducts.component'
import { EditProduct } from './Products/EditProduct/EditProduct.component';
import { SearchProduct } from './Products/SearchProduct/SearchProduct.component';
import ForgotPassword from './Auth/ForgotPassword/ForgotPassword.component';
import { ResetPassword } from './Auth/ResetPassword/ResetPassword.component';
import MessageComponent from './Users/Messages/Messages.component';
import { ProductDetailsLanding } from './Products/ProductDetails/ProductDetails.landing';

const Home = (props) => {
    return <p>Home Page</p>
}

const Dashboard = (props) => {
    console.log('props in dashboard ', props)
    return (
        <>
            <h2>Welcome</h2>
            <p>Welcome to Our Store please use side navigation menu or contact system administrator for support!</p>
        </>)
}
const About = (props) => {
    console.log('props  in about>>', props)
    return <p>About Page</p>
}
const Contact = (props) => {
    return <p>Contact Page</p>
}
const Settings = (props) => {
    return <p>Settings Page</p>
}

const NotFound = (props) => {
    return (
        <div>
            <p>Not Found</p>
            <img src="/images/notfound.jpeg" alt="notfound.png"></img>
        </div>
    )
}

const ProtectedRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={(routeProps) => {
        return localStorage.getItem('token')
            ? <>
                <Header isLoggedIn={true}></Header>
                <Sidebar isLoggedIn={true} />
                <div className="main">
                    <Component {...routeProps} ></Component>
                </div>
            </>
            : <Redirect to="/" >  </Redirect>
    }}></Route>
}

const PublicRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={(routeProps) => (
        <>
            <Header isLoggedIn={localStorage.getItem('token') ? true : false}></Header>
            <Sidebar isLoggedIn={localStorage.getItem('token') ? true : false}></Sidebar>
            <div className="main">
                <Component {...routeProps} ></Component>
            </div>
        </>
    )}></Route >
}

const AuthRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={(routeProps) => (
        <>
            <Header isLoggedIn={false}></Header>
            <div className="main">
                <Component {...routeProps} ></Component>
            </div>
        </>
    )}></Route >

}






export const AppRouting = (props) => {
    return (
        <BrowserRouter>
            <Switch>
                <AuthRoute path="/" exact component={Login}></AuthRoute>
                <AuthRoute path="/register" component={Register} />
                <AuthRoute path="/forgot_password" component={ForgotPassword} />
                <AuthRoute path="/reset_password/:token" component={ResetPassword} />
                <PublicRoute path="/home" exact component={Home} />
                <ProtectedRoute path="/about" component={About} />
                <ProtectedRoute path="/contact" component={Contact} />
                <ProtectedRoute path="/settings" component={Settings} />
                <ProtectedRoute path="/dashboard" component={Dashboard}></ProtectedRoute>
                <ProtectedRoute path="/add_product" component={AddProduct}></ProtectedRoute>
                <ProtectedRoute path="/view_products" component={ViewProducts}></ProtectedRoute>
                <ProtectedRoute path="/edit_product/:id" component={EditProduct}></ProtectedRoute>
                <ProtectedRoute path="/messages" component={MessageComponent}></ProtectedRoute>
                <PublicRoute path="/search_product" component={SearchProduct}></PublicRoute>
                <PublicRoute path="/product_details/:id" component={ProductDetailsLanding}></PublicRoute>
                <PublicRoute component={NotFound} />
            </Switch>

        </BrowserRouter>
    )
}


// route props
// history
// match
// location

// summary
// library ==> react router dom
// BrowserRouter==> wrapper to provide scope of routing
// Route ==> configuration block of path and component
// Link/NavLink  ==> navigation on mouse click event
// to use link and navlink we must be inside browserrouter
// exact flag
// dynmic url handler
// props ==> 
// history ==> navigation functions
// match  ==> params ==> dynamic url value
// location ==> optioal url values  and routing data
// swtich ==> incase we have registered route don't show default component

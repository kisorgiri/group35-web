import React from 'react'; // 17 dekhi optional cha
import { AppRouting } from './app.routing';
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { Provider } from 'react-redux';
import { store } from '../store';

export const App = (props) => {
    return (
        <div>
            <Provider store={store}>
                <AppRouting />
            </Provider>
            <ToastContainer />
        </div>

    )
}






// component 
// component is basic building block of react
// component always returns single html node
// component can be customized to define desired behaviour

// component can be statefull and stateless

// component can be written as
// functional component
// class based component

//  < 16.8
// class for statefull component
// functions for stateless component

// > 16.8 (hooks)
// functional component can be statefull

// thorough this course
// class for statefull component
// functions for stateless component

// hooks will be introduced as well as implemented


// glossary
// props ==> incoming data for a component
// eg attributes sent when calling a component are props for that component
// state ==> data within a component

// props and state are data for a component

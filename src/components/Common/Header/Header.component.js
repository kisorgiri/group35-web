import React from 'react';
import './Header.component.css'
import { NavLink, withRouter } from 'react-router-dom'

const logout = (history) => {
    // clear localstorage
    localStorage.clear();
    // navigate to login
    history.push('/')

}
const HeaderComponent = (props) => {
    const currentUser = JSON.parse(localStorage.getItem('user'));
    let menu = props.isLoggedIn
        ? <ul className="navList">
            <li className="navItem">
                <NavLink activeClassName="selected" to="/dashboard">Dashboard</NavLink>
            </li>
            <li className="navItem">
                <NavLink activeClassName="selected" to="/about">About</NavLink>

            </li>
            <li className="navItem">
                <NavLink activeClassName="selected" to="/contact">Contact</NavLink>

            </li>
            <li className="navItem">
                <NavLink activeClassName="selected" to="/settings">Settings</NavLink>

            </li>
            <li className="navItem">
                {/* <NavLink activeClassName="selected" exact to="/">Logout</NavLink> */}
                <button className="btn btn-info logout" onClick={() => logout(props.history)}>logout</button>
                <p className="user-info">{currentUser.username}</p>

            </li>
        </ul>
        : <ul className="navList">
            <li className="navItem">
                <NavLink activeClassName="selected" to="/home">Home</NavLink>

            </li>
            <li className="navItem">
                <NavLink activeClassName="selected" exact to="/">Login</NavLink>

            </li>
            <li className="navItem">
                <NavLink activeClassName="selected" to="/register">Register</NavLink>
            </li>
        </ul>

    return (
        <div className="navBar">
            {menu}
        </div>
    )
}

export const Header = withRouter(HeaderComponent)

import React from 'react';
import { NavLink } from 'react-router-dom';
import './Sidebar.component.css';

export const Sidebar = (props) => {
    let content = props.isLoggedIn
        ? <div className="sidebar">
            <NavLink activeClassName="sidebar-selected" to="/add_product">Add Product</NavLink>
            <NavLink activeClassName="sidebar-selected" to="/view_Products">View Product</NavLink>
            <NavLink activeClassName="sidebar-selected" to="/search_product">Search Product</NavLink>
            <hr></hr>
            <NavLink activeClassName="sidebar-selected" to="/notification">Notifications</NavLink>
            <NavLink activeClassName="sidebar-selected" to="/messages">Messages</NavLink>
        </div>
        : null;
    return content;
}

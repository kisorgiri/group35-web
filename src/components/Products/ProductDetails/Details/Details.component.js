import React, { Component } from 'react'
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";
import StarRatings from 'react-star-ratings';
import { realativeTime } from './../../../../utils/dateUtil';

const IMG_URL = process.env.REACT_APP_IMG_URL;
const images = [
    {
        original: 'https://picsum.photos/id/1018/1000/600/',
        thumbnail: 'https://picsum.photos/id/1018/250/150/',
    },
    {
        original: 'https://picsum.photos/id/1015/1000/600/',
        thumbnail: 'https://picsum.photos/id/1015/250/150/',
    },
    {
        original: 'https://picsum.photos/id/1019/1000/600/',
        thumbnail: 'https://picsum.photos/id/1019/250/150/',
    },
];

export class Details extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }

    getImages = () => {
        const { product } = this.props;
        const images = (product.images || []).map((img, index) => ({
            original: `${IMG_URL}/${img}`,
            thumbnail: 'https://picsum.photos/id/1018/250/150/'
        }))
        return images;
    }


    render() {
        const { product } = this.props;
        return (
            <>
                <div className="row">
                    <div className="col-md-6">
                        <ImageGallery items={this.getImages()} />
                    </div>
                    <div className="col-md-6">
                        <h2>{product.name}</h2>
                        <p>Description: {product.description}</p>
                        <p>Category: {product.Category}</p>
                        <p>.........</p>
                        {product.reviews && product.reviews.length > 0 && (
                            <div style={{ height: '300px', overflowY: 'auto' }}>
                                <h2>Reviews</h2>
                                {
                                    product.reviews.map((review, index) => (
                                        <div key={index}>
                                            <StarRatings
                                                rating={review.point}
                                                starRatedColor="blue"
                                                disabled={true}
                                                numberOfStars={5}
                                            />
                                            <p>{review.message}</p>
                                            <p>{review.user.username}</p>
                                            <small>{realativeTime(review.createdAt)}</small>
                                        </div>
                                    ))
                                }
                            </div>
                        )}

                    </div>
                </div>
            </>
        )
    }
}

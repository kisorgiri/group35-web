import React, { Component } from 'react'
import { connect } from 'react-redux';
import { addReview_ac, fetchProductById_ac, fetchReview_ac } from '../../../actions/products/product.ac';
import { Details } from './Details/Details.component';
import { ReviewForm } from './ReviewForm/ReviewForm.component';

class ProductDetailsLandingComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }

    componentDidMount() {
        this.productId = this.props.match.params['id'];
        this.props.fetch_by_id(this.productId);
        this.props.fetch_review(this.productId)
    }

    addReview = (data) => {
        this.props.add_review(this.productId, data);
    }
    render() {
        return (
            <>
                <Details
                    isLoading={this.props.isLoading}
                    product={this.props.product}
                ></Details>
                <ReviewForm
                    addReview={this.addReview}
                    isSubmitting={this.props.isSubmitting}
                ></ReviewForm>
            </>

        )
    }
}
const mapStateToProps = rootStore => ({
    product: rootStore.product.product,
    isLoading: rootStore.product.isLoading,
    isSubmitting: rootStore.product.isSubmitting
})

const mapDispatchToProps = dispatch => ({
    fetch_by_id: (id) => dispatch(fetchProductById_ac(id)),
    add_review: (id, data) => dispatch(addReview_ac(id, data)),
    fetch_review: (id) => dispatch(fetchReview_ac(id))
})

export const ProductDetailsLanding = connect(mapStateToProps, mapDispatchToProps)(ProductDetailsLandingComponent)

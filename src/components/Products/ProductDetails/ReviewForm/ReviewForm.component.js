import React, { Component } from 'react'
import StarRatings from 'react-star-ratings';
import { SubmitButton } from '../../../Common/Button/Button.component';

const defaultForm = {
    reviewPoint: 1,
    reviewMessage: ''
}

export class ReviewForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: {
                ...defaultForm
            }
        }
    }

    changeRating = (newRating, name) => {
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: newRating
            }
        }));
    }
    handleChange = e => {
        const { name, value } = e.target;

        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }));
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.addReview(this.state.data)
        this.setState({
            data: {
                ...defaultForm
            }
        })
    }

    render() {
        return (
            <>
                <h2>Add Review</h2>
                <form onSubmit={this.handleSubmit} className="form-group" noValidate>
                    <label>Point</label>
                    <br />
                    <StarRatings
                        rating={this.state.data.reviewPoint}
                        starRatedColor="blue"
                        changeRating={this.changeRating}
                        numberOfStars={5}
                        name='reviewPoint'
                    />
                    <br />
                    <label>Message</label>
                    <input type="text" name="reviewMessage" value={this.state.data.reviewMessage} placeholder="message here" className="form-control" onChange={this.handleChange}></input>
                    <hr />
                    <SubmitButton isSubmitting={this.props.isSubmitting}></SubmitButton>
                </form>
            </>
        )
    }
}

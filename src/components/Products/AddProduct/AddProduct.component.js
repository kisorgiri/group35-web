import React, { Component } from 'react'
import { handleError } from '../../../utils/errorHandler'
import { httpClient } from '../../../utils/httpClient'
import { notify } from '../../../utils/toastr'
import { ProductForm } from '../ProductForm/ProductForm.component'

export class AddProduct extends Component {
    constructor() {
        super()

        this.state = {
            isSubmitting: false
        }
    }

    add = (data, files) => {
        this.setState({
            isSubmitting: true
        })
        httpClient.UPLOAD('POST','/product', data, files)
            .then(response => {
                notify.showSuccess('Product Added successfully');
                this.props.history.push('/view_products')
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }


    render() {
        return (
            <>
                <ProductForm
                    isEditMode={false}
                    isSubmitting={this.state.isSubmitting}
                    submitCallback={this.add}
                >

                </ProductForm>

            </>
        )
    }
}

import React, { Component } from 'react'
import { handleError } from '../../../utils/errorHandler';
import { httpClient } from '../../../utils/httpClient';
import { notify } from '../../../utils/toastr';
import { Loader } from '../../Common/Loader/Loader.component';
import { ProductForm } from '../ProductForm/ProductForm.component';

export class EditProduct extends Component {
    constructor() {
        super()

        this.state = {
            product: {},
            isLoading: false,
            isSubmitting: false
        }
    }
    componentDidMount() {
        console.log('here at aedit')
        // extract product id form url
        this.setState({
            isLoading: true
        })
        this.productId = this.props.match.params['id'];
        httpClient.GET(`/product/${this.productId}`, true)
            .then(response => {
                this.setState({
                    product: response.data
                })
            })
            .catch(err => {
                handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }

    edit = (data, filesToUpload, filesToRemove) => {
        this.setState({
            isSubmitting: true
        })
        httpClient.UPLOAD('PUT', `/product/${this.productId}`, data, filesToUpload, filesToRemove)
            .then(response => {
                notify.showInfo("Product Udpated Successfully");
                this.props.history.push('/view_products')
            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        let content = this.state.isLoading
            ? <Loader />
            : <ProductForm
                isEditMode={true}
                submitCallback={this.edit}
                isSubmitting={this.state.isSubmitting}
                productData={this.state.product}
            >

            </ProductForm>

        return content;
    }
}

import React, { Component } from 'react'
import { withRouter } from 'react-router';
import { formatDate } from '../../../utils/dateUtil'
import { SubmitButton } from './../../Common/Button/Button.component'
const IMG_URL = process.env.REACT_APP_IMG_URL;

const defaultForm = {
    name: '',
    price: '',
    colors: '',
    brand: '',
    category: '',
    quantity: '',
    quality: '',
    tags: '',
    offers: '',
    warrentyStatus: '',
    warrentyPeroid: '',
    discountedItem: '',
    discountType: '',
    discountValue: '',
    size: '',
    purchasedDate: '',
    isReturnEligible: '',
}
class ProductFormComponent extends Component {
    constructor() {
        super()

        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            filesToUpload: [],
            previousSelectedFiles: [],
            filesToRemove: [],
        }
    }
    componentDidMount() {
        // check props
        console.log("props in commmon commponent >>", this.props)
        const { productData } = this.props;
        if (productData) {
            this.setState({
                data: {
                    ...defaultForm,
                    ...productData,
                    discountedItem: productData.discount && productData.discount.discountedItem
                        ? productData.discount.discountedItem
                        : false,
                    discountType: productData.discount && productData.discount.discountType
                        ? productData.discount.discountType
                        : '',
                    discountValue: productData.discount && productData.discount.discountValue
                        ? productData.discount.discountValue
                        : '',
                    purchasedDate: productData.purchasedDate
                        ? formatDate(productData.purchasedDate, 'YYYY-MM-DD')
                        : ''
                },
                previousSelectedFiles: (productData.images || []).map((img, index) => {
                    return `${IMG_URL}/${img}`
                })

            })
        }
    }

    handleChange = (e) => {
        let { name, type, value, checked, files } = e.target;

        if (type === 'checkbox') {
            value = checked;
        }

        if (type === 'file') {
            const { filesToUpload } = this.state;
            filesToUpload.push(files[0]);
            this.setState({
                filesToUpload
            })
        }
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            if (this.state.error[name]) {
                this.validateForm();
            }
        })

    }

    validateForm = fieldName => {
        let isValidForm = true;
        let nameErr = false;
        let categoryErr = false;

        if (!this.state.data['name']) {
            nameErr = true;
            isValidForm = false;
        }
        if (!this.state.data['category']) {
            categoryErr = true;
            isValidForm = false;
        }

        this.setState({
            error: {
                name: nameErr,
                category: categoryErr
            }
        })
        return isValidForm;
    }

    handleSubmit = e => {
        e.preventDefault();
        const isValid = this.validateForm();
        if (!isValid) return;
        this.props.submitCallback(this.state.data, this.state.filesToUpload, this.state.filesToRemove);
    }

    removeSelectedImage = (file, index) => {
        const { filesToUpload } = this.state;
        filesToUpload.splice(index, 1);
        this.setState({
            filesToUpload
        })
    }

    removePreviousImage = (file, index) => {
        const { filesToRemove, previousSelectedFiles } = this.state;
        previousSelectedFiles.splice(index, 1);
        filesToRemove.push(file);
        this.setState({
            filesToRemove,
            previousSelectedFiles
        })
    }

    render() {
        return (
            <>
                <h2>{this.props.isEditMode ? 'Update' : 'Add'} Product</h2>
                <p>Please fill all the required fields</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Name</label>
                    <input type="text" name="name" value={this.state.data.name} placeholder="Name" className="form-control" onChange={this.handleChange}></input>
                    <p className="error"> {this.state.error.name && 'required field*'}</p>
                    <label>Description</label>
                    <textarea rows={8} name="description" value={this.state.data.description} placeholder="Description here" className="form-control" onChange={this.handleChange}></textarea>
                    <label>Category</label>
                    <input type="text" name="category" value={this.state.data.category} placeholder="Category" className="form-control" onChange={this.handleChange}></input>
                    <p className="error"> {this.state.error.category && 'required field*'}</p>

                    <label>Brand</label>
                    <input type="text" name="brand" value={this.state.data.brand} placeholder="Brand" className="form-control" onChange={this.handleChange}></input>
                    <label>Colors</label>
                    <input type="text" name="colors" value={this.state.data.colors} placeholder="Colors" className="form-control" onChange={this.handleChange}></input>
                    <label>Price</label>
                    <input type="number" name="price" value={this.state.data.price} className="form-control" onChange={this.handleChange}></input>
                    <label>Quantity</label>
                    <input type="number" name="quantity" value={this.state.data.quantity} className="form-control" onChange={this.handleChange}></input>
                    <label>Quality</label>
                    <select name="quality" className="form-control" value={this.state.data.quality} onChange={this.handleChange}>
                        <option disabled value="">(Select Quality)</option>
                        <option value="high">High</option>
                        <option value="medium">Medium</option>
                        <option value="">Low</option>
                    </select>
                    <label>Size</label>
                    <input type="text" name="size" value={this.state.data.size} placeholder="Size" className="form-control" onChange={this.handleChange}></input>
                    <input type="checkbox" checked={this.state.data.isReturnEligible} name="isReturnEligible" onChange={this.handleChange}></input>
                    <label>&nbsp;Is Return Eligible</label>
                    <br />
                    <label>Tags</label>
                    <input type="text" name="tags" value={this.state.data.tags} placeholder="Tags" className="form-control" onChange={this.handleChange}></input>
                    <label>Offers</label>
                    <input type="text" name="offers" value={this.state.data.offers} placeholder="Offers" className="form-control" onChange={this.handleChange}></input>
                    <label>Purchased Date</label>
                    <input type="date" name="purchasedDate" value={this.state.data.purchasedDate} className="form-control" onChange={this.handleChange}></input>
                    <input type="checkbox" checked={this.state.data.warrentyStatus} name="warrentyStatus" onChange={this.handleChange}></input>
                    <label> &nbsp;Warrenty Status</label>
                    {
                        this.state.data.warrentyStatus && (
                            <>
                                <br />
                                <label>Warrenty Peroid</label>
                                <input type="text" value={this.state.data.warrentyPeroid} name="warrentyPeroid" placeholder="Warrenty Peroid" className="form-control" onChange={this.handleChange}></input>
                            </>
                        )
                    }
                    <br />
                    <input type="checkbox" checked={this.state.data.discountedItem} name="discountedItem" onChange={this.handleChange}></input>
                    <label> &nbsp;Discounted Item</label>
                    {
                        this.state.data.discountedItem && (
                            <>
                                <br />
                                <label>Discount Type</label>
                                <select type="text" name="discountType" value={this.state.data.discountType} placeholder="Discount Type" className="form-control" onChange={this.handleChange}>
                                    <option value="">(Select Type)</option>
                                    <option value="percentage">Percentage</option>
                                    <option value="quantity">Quantity</option>
                                    <option value="value">Value</option>
                                </select>
                                <label>Discount Value</label>
                                <input type="text" value={this.state.data.discountValue} name="discountValue" placeholder="Discount Value" className="form-control" onChange={this.handleChange}></input>
                            </>
                        )
                    }
                    <label>Choose Image</label>
                    <input type="file" className="form-control" onChange={this.handleChange}></input>
                    {
                        this.state.filesToUpload.map((file, index) => (
                            <div key={index} style={{ marginTop: '10px' }}>
                                <img src={URL.createObjectURL(file)} alt="selected_img.png" width="200px"></img>
                                <i
                                    className="fa fa-trash"
                                    style={{ marginLeft: '10px', color: 'red', cursor: 'pointer' }}
                                    onClick={() => this.removeSelectedImage(file, index)}
                                ></i>
                            </div>
                        ))
                    }
                    {
                        this.state.previousSelectedFiles.map((img_url, index) => (
                            <div key={index} style={{ marginTop: '10px' }}>
                                <img src={img_url} alt="selected_img.png" width="200px"></img>
                                <i
                                    className="fa fa-trash"
                                    style={{ marginLeft: '10px', color: 'red', cursor: 'pointer' }}
                                    onClick={() => this.removePreviousImage(img_url, index)}
                                ></i>
                            </div>
                        ))
                    }
                    <hr />
                    <SubmitButton
                        isSubmitting={this.props.isSubmitting}
                    >
                    </SubmitButton>
                    <a style={{ marginLeft: '10px' }} className="btn btn-warning" onClick={() => this.props.history.goBack()}>Cancel</a>

                </form>
            </>
        )
    }
}


export const ProductForm = withRouter(ProductFormComponent)

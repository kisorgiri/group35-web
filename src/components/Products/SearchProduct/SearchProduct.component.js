import React, { Component } from 'react'
import { handleError } from '../../../utils/errorHandler'
import { httpClient } from '../../../utils/httpClient'
import { notify } from '../../../utils/toastr'
import { SubmitButton } from '../../Common/Button/Button.component'
import { ViewProducts } from '../ViewProducts/ViewProducts.component'

const defatulForm = {
    category: '',
    name: '',
    brand: '',
    minPrice: '',
    maxPrice: '',
    fromDate: '',
    toDate: '',
    tags: '',
    multipleDateRange: false
}

export class SearchProduct extends Component {
    constructor() {
        super()

        this.state = {
            data: { ...defatulForm },
            error: { ...defatulForm },
            isSubmitting: false,
            categories: [],
            allProducts: [],
            names: [],
            searchResults: []
        }
    }
    componentDidMount() {
        httpClient.POST('/product/search', {})
            .then(response => {
                let categories = [];
                response.data.forEach((item, index) => {
                    if (!categories.includes(item.category)) {
                        categories.push(item.category)
                    }
                })
                this.setState({
                    categories: categories,
                    allProducts: response.data
                })
            })
            .catch(err => {
                handleError(err);
            })

    }

    handleChange = e => {

        let { name, value, type, checked } = e.target;
        if (type === 'checkbox') {
            value = checked
        }
        if (name === 'category') {
            this.prepareNames(value);
        }
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            // todo
        })

    }

    prepareNames = selectedCategory => {
        let names = this.state.allProducts.filter(product => product.category === selectedCategory);
        this.setState({
            names: names
        })
    }

    handleSubmit = e => {
        this.setState({
            isSubmitting: true
        })
        const { data } = this.state;
        if (!data.multipleDateRange) {
            data.toDate = data.fromDate;
        }
        e.preventDefault();
        httpClient.POST('/product/search', data)
            .then(response => {
                if (!response.data.length) {
                    return notify.showInfo("No any product matched your search query")
                }
                this.setState({
                    searchResults: response.data
                })
            })
            .catch(err => {
                handleError(err);

            })
            .finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })
    }
    resetSearch = () => {
        this.setState({
            searchResults: [],
            data: {
                ...defatulForm
            }
        })
    }

    render() {
        let content = this.state.searchResults.length
            ? <ViewProducts
                resetSearch={this.resetSearch}
                productData={this.state.searchResults}
                {...this.props}
            ></ViewProducts>
            : <>
                <h2>Search Product</h2>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Category</label>
                    <select name="category" className="form-control" value={this.state.data.category} onChange={this.handleChange}>
                        <option value="">(Select Category)</option>
                        {this.state.categories.map((cat, i) => (
                            <option key={i} value={cat}>{cat}</option>
                        ))}
                    </select>
                    {this.state.names.length > 0 && (
                        <>
                            <label>Name</label>
                            <select name="name" className="form-control" value={this.state.data.name} onChange={this.handleChange}>
                                <option value="">(Select Name)</option>
                                {this.state.names.map((item, i) => (
                                    <option key={item._id} value={item.name}>{item.name}</option>
                                ))}
                            </select>

                        </>
                    )}

                    <label>Brand</label>
                    <input type="text" name="brand" className="form-control" placeholder="Brand" onChange={this.handleChange}></input>
                    <label>Color</label>
                    <input type="text" name="color" className="form-control" placeholder="Color" onChange={this.handleChange}></input>
                    <label>Min Price</label>
                    <input type="number" name="minPrice" className="form-control" onChange={this.handleChange}></input>
                    <label>Max Price</label>
                    <input type="number" name="maxPrice" className="form-control" onChange={this.handleChange}></input>
                    <label>Select Date</label>
                    <input type="date" name="fromDate" className="form-control" onChange={this.handleChange}></input>
                    <input type="checkbox" name="multipleDateRange" onChange={this.handleChange}></input>
                    <label>&nbsp;Multiple Date Range</label>
                    <br />
                    {
                        this.state.data.multipleDateRange && (
                            <>
                                <label>To Date</label>
                                <input type="date" name="toDate" className="form-control" onChange={this.handleChange}></input>
                            </>
                        )
                    }

                    <label>Tags</label>
                    <input type="text" name="tags" className="form-control" placeholder="Tags" onChange={this.handleChange}></input>
                    <hr />
                    <SubmitButton
                        isSubmitting={this.state.isSubmitting}
                    />

                </form>
            </>
        return content;
    }
}

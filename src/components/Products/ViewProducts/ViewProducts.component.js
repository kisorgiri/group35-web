import React, { Component } from 'react'
import { formatDate } from './../../../utils/dateUtil';
import { Link } from 'react-router-dom';
import { Loader } from '../../Common/Loader/Loader.component';
import { connect } from 'react-redux'
import { changePageNumber_ac, fetcProducts_ac, removeProduct_ac } from '../../../actions/products/product.ac';

const IMG_URL = process.env.REACT_APP_IMG_URL;
class ViewProductsComponent extends Component {
    constructor() {
        super()
    }

    componentDidMount() {
        console.log('props is >>', this.props)
        if (this.props.productData) {
            return this.setState({
                products: this.props.productData
            })
        }
        this.props.fetch({
            pageNumber: this.props.pageNumber,
            pageSize: this.props.pageSize
        });

    }

    componentDidUpdate() {
        console.log('component updated');
    }

    editProduct = (id) => {
        this.props.history.push(`/edit_product/${id}`)
    }

    removeProduct = (id) => {
        // ask for confirmation
        // search for modal(dialog) in awesome react components
        // use one of them for popup
        const confirmation = window.confirm('Are you sure to remove?');
        if (confirmation) {
            this.props.removeProduct_ac(id);
        }
    }

    changePageNumber = (evt) => {
        let { pageNumber, pageSize } = this.props
        if (evt === 'next') {
            pageNumber++;
        }
        if (evt === 'previous') {
            pageNumber--;
        }
        this.props.fetch({
            pageNumber: pageNumber,
            pageSize: pageSize
        });
        this.props.changePageNumber_ac(pageNumber)
    }

    render() {
        let content = this.props.isLoading
            ? <Loader />
            : <table className="table">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Price</th>
                        <th>Created At</th>
                        <th>Tags</th>
                        <th>Images</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        this.props.products.map((product, index) => (
                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td> <Link to={`/product_details/${product._id}`}>{product.name} </Link> </td>
                                <td>{product.category}</td>
                                <td>{product.price}</td>
                                <td>{product.tags.length ? product.tags.join(',') : 'N/A'}</td>
                                <td>{formatDate(product.createdAt)}</td>
                                <td>
                                    <img src={`${IMG_URL}/${product.images[0]}`} alt="proudct_image.png" width="150px"></img>
                                </td>
                                <td>
                                    <i
                                        className="fa fa-pencil"
                                        style={{ color: 'blue', cursor: 'pointer' }}
                                        onClick={() => this.editProduct(product._id)}
                                    ></i> |
                                    <i
                                        className="fa fa-trash"
                                        style={{ color: 'red', cursor: 'pointer' }}
                                        onClick={() => this.removeProduct(product._id)}
                                    ></i>
                                </td>
                            </tr>
                        ))
                    }
                    {
                        this.props.pageNumber !== 1 && (
                            <button className="btn btn-success" onClick={() => this.changePageNumber('previous')}>Previous</button>
                        )
                    }
                    <button className="btn btn-success" onClick={() => this.changePageNumber('next')}>Next</button>

                </tbody>
            </table>
        return (
            <>
                <h2>View Products</h2>
                {this.props.productData && (
                    <button className="btn btn-success" onClick={() => this.props.resetSearch()}>Search Again</button>
                )}
                {content}
            </>
        )
    }
}

// incoiming data from root store as an props
const mapStateToProps = rootStore => ({
    isLoading: rootStore.product.isLoading,
    products: rootStore.product.products,
    pageNumber: rootStore.product.pageNumber,
    pageSize: rootStore.product.pageSize
})

// actions to be imported inside component in props
const mapDispatchToProps = dispatch => ({
    fetch: (params) => dispatch(fetcProducts_ac(params)),
    removeProduct_ac: (id) => dispatch(removeProduct_ac(id)),
    changePageNumber_ac: (pageNumber) => dispatch(changePageNumber_ac(pageNumber))
})

export const ViewProducts = connect(mapStateToProps, mapDispatchToProps)(ViewProductsComponent)

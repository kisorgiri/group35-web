import React, { Component } from 'react'
import * as io from 'socket.io-client';
import { realativeTime } from '../../../utils/dateUtil';
import { notify } from '../../../utils/toastr';
import './Messages.component.css'

export default class MessageComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            msgBody: {
                senderId: '',
                senderName: '',
                receiverId: '',
                receiverName: '',
                message: '',
                time: ''
            },
            messages: [],
            users: [], //db users
            isTyping: false
        }
    }
    componentDidMount() {
        this.socket = io(process.env.REACT_APP_SOCKET_URL);
        this.currentUser = JSON.parse(localStorage.getItem('user'));
        this.runSocket();
    }

    componentWillUnmount() {
        this.socket.emit('disconnect-client', this.currentUser.username);
    }


    runSocket() {

        this.socket.emit('new-user', this.currentUser.username);

        this.socket.on('reply-message-own', data => {
            const { messages } = this.state;
            messages.push(data);
            this.setState({
                messages
            })
        })
        this.socket.on('reply-message-client', data => {
            const { messages, msgBody } = this.state;
            // swap sender and receiver
            msgBody.receiverId = data.senderId;
            msgBody.receiverName = data.senderName;
            messages.push(data);
            this.setState({
                messages,
                msgBody
            })
        })

        this.socket.on('is-typing', state => {
            console.log('check typing sate >', state)
            this.setState({
                isTyping: state
            })
        })

        this.socket.on('users', (users) => {
            this.setState({
                users: users
            })
        })
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            msgBody: {
                ...preState.msgBody,
                [name]: value
            }
        }))
    }

    send = e => {
        e.preventDefault();

        const { msgBody, users } = this.state;
        // append data in msgbody
        if (!msgBody.receiverId) {
            return notify.showInfo("Please select a user to continue")
        }
        msgBody.time = Date.now();
        msgBody.senderName = this.currentUser.username;
        const senderDetails = users.find(user => user.name === this.currentUser.username);
        msgBody.senderId = senderDetails.id;
        this.socket.emit('new-message', msgBody)
        this.setState(preState => ({
            msgBody: {
                ...preState.msgBody,
                message: ''
            }
        }))
    }

    selectUser = user => {
        this.setState(preState => ({
            msgBody: {
                ...preState.msgBody,
                receiverId: user.id,
                receiverName: user.name
            }
        }))
    }
    focus = () => {
        const { msgBody } = this.state;
        if (msgBody.senderId && msgBody.receiverId) {

            this.socket.emit('is-typing', this.state.msgBody)
        }
    }
    blur = () => {
        this.socket.emit('is-typing-stop', this.state.msgBody)
    }
    render() {
        return (
            <>
                <h2>Messages</h2>
                <p>Let's Chat</p>
                <div className="row">
                    <div className="col-md-8">
                        <p><strong>Messages</strong></p>
                        <div className="message-box">
                            {this.state.messages.map((msg, index) => (
                                <div key={index} className="reply-msg">
                                    <p> <strong>{msg.senderName}</strong></p>
                                    <p>{msg.message}</p>
                                    <small> {realativeTime(msg.time, 'minute')}</small>
                                </div>
                            ))}
                        </div>
                        <div style={{ marginTop: '10px' }}>
                            <form onSubmit={this.send} className="form-group">
                                <input type="text" onFocus={this.focus} onBlur={this.blur} placeholder="your message here..." value={this.state.msgBody.message} name="message" onChange={this.handleChange} className="form-control input-message"></input>
                                <p>{this.state.isTyping && ` ${this.state.msgBody.receiverName} is Typing...`}</p>
                                <button
                                    className="btn btn-success"
                                    type="submit">
                                    send
                                </button>
                            </form>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <p><strong>Users</strong></p>
                        <div className="message-box">
                            {this.state.users.map((user, index) => (
                                <button
                                    style={{ display: 'block' }}
                                    key={index}
                                    className="btn btn-default"
                                    onClick={() => this.selectUser(user)}
                                >{user.name}</button>
                            ))}
                        </div>
                    </div>

                </div>

            </>
        )
    }
}

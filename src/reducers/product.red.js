import { ProductActionTypes } from "../actions/products/product.ac";

const defaultState = {
    isLoading: false,
    products: [],
    product: {}
};


export const productReducer = (state = defaultState, action) => {
    console.log('at reducer with action ', action)
    switch (action.type) {
        case ProductActionTypes.SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }
        case ProductActionTypes.PRODUCTS_RECEIVED:
            return {
                ...state,
                products: action.payload
            }
        case ProductActionTypes.PRODUCT_RECEIVED:
            return {
                ...state,
                product: action.product
            }
        case ProductActionTypes.SET_PAGE_NUMBER:
            return {
                ...state,
                pageNumber: action.payload
            }
        case ProductActionTypes.PRODUCT_REMOVED:
            const { products } = state;
            products.forEach((prod, i) => {
                if (prod._id === action.productId) {
                    products.splice(i, 1);
                }
            })
            return {
                ...state,
                products: [...products]
            }
        case ProductActionTypes.SET_IS_SUBMITTING:
            return {
                ...state,
                isSubmitting: action.isSubmitting
            }

        case ProductActionTypes.REVIEW_RECEIVED:
            const { product } = state;
            if (product._id === action.productId) {
                product.reviews = action.payload
            }
            return {
                ...state,
                product: { ...product }
            }
        default:
            return {
                ...state
            }
    }
}

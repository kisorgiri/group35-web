import axios from 'axios';
const BASE_URL = process.env.REACT_APP_BASE_URL;

const http = axios.create({
    baseURL: BASE_URL,
    responseType: 'json',
    timeout: 10000,
    timeoutErrorMessage: 'Server Unreachable'
})

const getHeaders = (secured) => {
    let options = {
        'Content-Type': 'application/json'
    }
    if (secured) {
        options['Authorization'] = `Bearer ${localStorage.getItem('token')}`
    }
    return options;
}

const GET = (url, isSecured = false, params = {}) => {
    return http.get(url, {
        headers: getHeaders(isSecured),
        params
    })
}

const POST = (url, data, isSecured = false, params = {}) => {
    return http.post(url, data, {
        headers: getHeaders(isSecured),
        params
    });
}

const PUT = (url, data, isSecured = false, params) => {
    return http.put(url, data, {
        headers: getHeaders(isSecured),
        params
    });
}

const DELETE = (url, isSecured = false, params) => {
    return http.delete(url, {
        headers: getHeaders(isSecured),
        params
    });
}

const UPLOAD = (method, url, data, files = [], filesToRemove = []) => {
    // file upload
    // xhr
    // formData
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        const formData = new FormData();

        // append files in formData
        if (files.length > 0) {
            files.forEach((file, index) => {
                formData.append('images', file, file.name)
            })
        }

        // add textual data in formData
        for (let key in data) {
            formData.append(key, data[key]);
        }

        if (filesToRemove.length) {
            formData.append('filesToRemove', filesToRemove)
        }

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(xhr.response);
                } else {
                    reject(xhr.response);
                }
            }

        }

        xhr.open(method, `${BASE_URL}/${url}?token=Bearer ${localStorage.getItem('token')}`, true);
        xhr.send(formData);
    })

}

export const httpClient = {
    GET,
    POST,
    DELETE,
    PUT,
    UPLOAD
}


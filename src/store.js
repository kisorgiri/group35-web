// store configuration
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers'

const middlewares = [thunk]

const inititalState = {
    product: {
        products: [],
        isLoading: false,
        pageNumber: 1,
        pageSize: 5,
        product: {},
        isSubmitting: false

    },
    // user: {

    //     users: [],
    //     isLoading: false,
    // },
    // notification: {
    //     notifications: [],
    //     isLoading: false
    // }
}

export const store = createStore(rootReducer, inititalState, applyMiddleware(...middlewares))

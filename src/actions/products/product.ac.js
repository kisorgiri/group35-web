import { handleError } from "../../utils/errorHandler";
import { httpClient } from "../../utils/httpClient";
import { notify } from "../../utils/toastr";

export const ProductActionTypes = {
    SET_IS_LOADING: 'SET_IS_LOADING',
    SET_IS_SUBMITTING: 'SET_IS_SUBMITTING',
    PRODUCTS_RECEIVED: 'PRODUCTS_RECEIVED',
    PRODUCT_RECEIVED: 'PRODUCT_RECEIVED',
    PRODUCT_REMOVED: 'PRODUCT_REMOVED',
    SET_PAGE_NUMBER: 'SET_PAGE_NUMBER',
    REVIEW_RECEIVED: 'REVIEW_RECEIVED'
}

// function fetch(params) {
//     return function (dispatch) {

//     }
// }

// export const fetcProducts_ac = (params) => {
//     return (dispatch) => {

//     }
// }

export const fetcProducts_ac = params => dispatch => {
    console.log('at action ', params);
    dispatch({
        type: ProductActionTypes.SET_IS_LOADING,
        payload: true
    })
    httpClient.GET('/product', true, params)
        .then(response => {
            dispatch({
                type: ProductActionTypes.PRODUCTS_RECEIVED,
                payload: response.data
            })
        })
        .catch(error => {
            handleError(error)
        })
        .finally(() => {
            dispatch({
                type: ProductActionTypes.SET_IS_LOADING,
                payload: false
            })
        })

}

export const removeProduct_ac = id => dispatch => {

    httpClient.DELETE(`/product/${id}`, true)
        .then(response => {
            dispatch({
                type: ProductActionTypes.PRODUCT_REMOVED,
                productId: id
            })
        })
        .catch(err => {
            handleError(err)
        })
}

export const fetchProductById_ac = id => dispatch => {
    dispatch({
        type: ProductActionTypes.SET_IS_LOADING,
        payload: true
    })
    httpClient.GET(`/product/${id}`, true)
        .then(response => {
            dispatch({
                type: ProductActionTypes.PRODUCT_RECEIVED,
                product: response.data
            })
        })
        .catch(error => {
            handleError(error)
        })
        .finally(() => {
            dispatch({
                type: ProductActionTypes.SET_IS_LOADING,
                payload: false
            })
        })
}

export const addReview_ac = (productId, data) => (dispatch, getState) => {

    // const { product } = getState();

    dispatch(submitting(true))
    httpClient.POST(`/review/${productId}`, data, true)
        .then(response => {
            notify.showInfo("Review Added");
            dispatch(fetchReview_ac(productId))
        })
        .catch(err => {
            handleError(err);
        })
        .finally(() => {
            dispatch(submitting(false))
        })
}

export const fetchReview_ac = (productId) => (dispatch) => {
    console.log('review fetch again')
    // dispatch(submitting(true))
    httpClient.GET(`/review/${productId}`, true)
        .then(response => {
            dispatch({
                type: ProductActionTypes.REVIEW_RECEIVED,
                payload: response.data,
                productId: productId
            })
        })
        .catch(err => {
            handleError(err);
        })
        .finally(() => {
            // dispatch(submitting(true))
        })
}

export const changePageNumber_ac = pageNumber => dispatch => {
    dispatch({
        type: ProductActionTypes.SET_PAGE_NUMBER,
        payload: pageNumber
    })
}

const submitting = isSubmitting => ({
    type: ProductActionTypes.SET_IS_SUBMITTING,
    isSubmitting
})

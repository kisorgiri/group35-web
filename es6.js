// grouping ES
// stable version es5
// modern JS es6 and beyond

// latest feature in ES
// 1.Object Shorthand
// 2.Object Destruction
// 3. Arrow notation function
// 4. Spread Operator
// 5.Rest Operator
// 6. Import and Export
// 7.template literals
// 8.default argument
// 9.class
// data types (symbol,bigint)


// 1.Object Shorthand

// object prepare
var email = 'jskiosr@gmail.com'
function welcome() {

}
var obj = {
    name: 'kishor',
    phone: 3333,
    email,
    welcome
}
console.log('obj is >>', obj)
// it will look for reference of email
// if found
// email will be key and its assigned value will value for object


// 1. Object Destruction
// function getMenu() {
let menu = {
    breakfast: ['fruits', 'coffee', 'bread'],
    drinks: 'juice',
    momo: 'chicken momo'
}
// return menu;
// }

let drinks = 'welcome drinks'

let { drinks: MyDrinks } = menu; //destructs

console.log('drinks >>', drinks)
console.log("changed name >>", MyDrinks)


// function sendMail({ from, to, sender: ABC }) {

// }
// sendMail({
//     sender: '',
//     receiver: '',
//     from: '',
//     to: ''
// })


// arrow notation functions

// function welcome(){

// }

// const welcome = ()=>{

// }

// function welcome(name) {
//     // 
// }

// const welcome = (name) => {

// }
// const welcome = name => {
//     // for single argument parenthesis is not mandatory
// }

// function multiply(num1, num2) {
//     return num1 * num2;
// }

// const multiply = (num1, num2) => {
//     return num1 * num2;
// }
// one liner function
const multiply = (num1, num2) => num1 * num2

const res = multiply(2, 4);
console.log('res is >>', res)

const laptops = [{
    name: 'xps',
    processor: 'i7',
    color: 'black'

},
{
    name: 'elitebook',
    processor: 'i5',
    color: 'red'

},
{
    name: 'mac',
    processor: 'i7',
    color: 'white'

},
{
    name: 'lenovo',
    processor: 'i7',
    color: 'red'

}]

// i7 laptops
const i7Laptops = laptops.filter(function (item) {
    if (item.processor === 'i7') {
        return item;
    }
})
console.log('i7laptops >>', i7Laptops);

const i7WithEs6 = laptops.filter(item => item.processor === 'i7')

console.log('i7 again >', i7WithEs6)

// main advantages
// function vanne keyword use garda functionle sandai aafnai this suru huncha
// arrow notation function will inherit parent this

// spread and rest operator
// ...

// spread
// ... is spread operator used with array and objects to spread each value

var ups = {
    name: 'prolink',
    price: 222,
    color: 'black'
}

var ups1 = { ...ups };

ups.name = 'abcd'
console.log('ups is >>', ups)
console.log('ups 1 is >>', ups1)
// copy values form array and objects

var anotherObj = {
    manuDate: 202020,
    origin: 'nepal',
    price: 111
}

const newObj = {
    ...ups,
    ...anotherObj
}

console.log('new obj >>', newObj)

// function sendMail(test) {
// ...test
// }

// sendMail({ ...test })

// rest with destruction
const details = {
    name: 'kishor',
    addr: 'bkt',
    phone: 222,
    emailone: 'jskisor@gmail.com'
}
const { emailone: Email, ...broadway } = details;

console.log('email is >', Email)
console.log('broadway is >>', broadway)


// import and export
// export
// two ways of export
// 1. named export
// 2. default export
// 1. named export 
// syntax
// export keyword
// eg. export class Test {}
// export const Hello = ()=>{}
// there can be multiple named export within a file

// 2. default export
// syntax
// export default [Array,String,number,boolean,function,object]
// there can be only one default export within a file

// there can be combination of named and default export from single file



// import
// import totatlly depends on how it is exported
// if named export 
// import syntax
// import { name1, name2 } from 'source module'

// if default export
// import any_name from 'source module'

// if both named and default export
// import Any_name, { name2, name3 } from 'module'

// template literals
// string 
// var message = 'welcome to broadway '+ email+'lskdf'+name
// template literals ``
var place = 'broadway'
var message = `welcome
to ${place} `

// default arguments

function ABC(email = 'jskisor@gmail.com') {
    console.log("email >>", email)
}

ABC('skdjflsdjf');

// class
// class is group of fiels, methods and constructor
// syntax
class WhiteBoard {
    category;

    constructor() {
        // constructor is self invoked function
        // value initilize
        this.category = 'stationary'
    }

    getCategory() {
        // class vitra function lekhda function vanne keyword chahidaina
        return this.category;
    }
}

// Inheritance
// base class (parent)
// derived class (child)

class Marker extends WhiteBoard {
    price;
    constructor() {
        super();
        // super call is parent class constructor call
        this.price = 332;
    }

    getPrice() {
        return this.price;
    }


}

// var a = new Marker();

// var b = new WhiteBoard();
// b.



